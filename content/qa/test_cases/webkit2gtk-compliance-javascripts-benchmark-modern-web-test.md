+++
date = "2016-06-09"
weight = 100

title = "webkit2gtk-compliance-javascripts-benchmark-modern-web-test"

aliases = [
    "/old-wiki/QA/Test_Cases/webkit2GTK-compliance-javascripts-benchmark-modern-web-test"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
