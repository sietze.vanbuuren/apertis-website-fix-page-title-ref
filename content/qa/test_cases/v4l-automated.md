+++
date = "2017-10-25"
weight = 100

title = "v4l-automated"

aliases = [
    "/old-wiki/QA/Test_Cases/v4l-automated"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
