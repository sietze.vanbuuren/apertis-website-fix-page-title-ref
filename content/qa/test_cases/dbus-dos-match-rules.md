+++
date = "2014-12-18"
weight = 100

title = "dbus-dos-match-rules"

aliases = [
    "/old-wiki/QA/Test_Cases/dbus-dos-match-rules"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
