+++
date = "2017-10-25"
weight = 100

title = "telepathy-gabble-unit"

aliases = [
    "/qa/test_cases/telepathy-gabble.md",
    "/old-wiki/QA/Test_Cases/telepathy-gabble",
    "/old-wiki/QA/Test_Cases/telepathy-gabble-unit"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
