+++
date = "2018-06-25"
weight = 100

title = "gstreamer1.0-decode"

aliases = [
    "/old-wiki/QA/Test_Cases/gstreamer1.0-decode",
    "/old-wiki/QA/Test_Cases/gstreamer1-0-decode",
    "/qa/test_cases/gstreamer1-0-decode.md"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
