+++
date = "2017-10-25"
weight = 100

title = "factory-reset"

aliases = [
    "/qa/test_cases/factory-reset-tool.md",
    "/old-wiki/QA/Test_Cases/factory-reset-tool",
    "/old-wiki/QA/Test_Cases/factory-reset"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
