+++
date = "2021-10-29"
lastmod = "2021-10-29"
weight = 100
toc = true

title = "i.MX8MN BSH SMM S2 PRO board"
+++

BSH SMM S2 PRO (SystemMaster S2 Pro) Board is an add-on board which provide input
and output interfaces to a dedicated carrier board. It is designed mainly to
provide graphical/video and connectivity interfaces to the appliance.


The MPU used for the BSH SMM S2 PRO is the
[i.MX 8M Nano](https://www.nxp.com/products/processors-and-microcontrollers/arm-processors/i-mx-applications-processors/i-mx-8-processors/i-mx-8m-nano-family-arm-cortex-a53-cortex-m7:i.MX8MNANO)
from NXP, which is a feature and performance scalable processor based on ARM
Cortex-A53 architecture with a Cortex M co-processor and optional graphic units.

![iMX8MN BSH SMM S2 PRO](/images/imx8mn_bsh_smm_s2pro_board.jpg)

This is the setup recommended for developers using the BSH SMM S2 PRO board. See
[LAVA boards setup](https://lava.pages.collabora.com/docs/boards/) for setting
up devices for use in LAVA.

# Required items

You should have at least:

- 1 iMX8MN BSH SMM S2 PRO board.
- 1 Power supply/adaptor 5V/4A DC, center positive.
- 2 USB type A to micro B cables.
- Cable to short 3.3V pin and boot mode pad.

# Board setup

This board model doesn't provide a SD card slot, so both U-Boot and Apertis
images are flashed into eMMC memory.

Board firmware is flashed using Freescale/NXP
[UUU tool](https://github.com/NXPmicro/mfgtools) over USB.

The first time, the board needs to be booted in *USB serial downloader* mode in
order to load and run U-Boot directly from memory. Then, U-Boot can be run in
UUU/fastboot mode, allowing the U-Boot and Apertis images to be flashed into
eMMC.

## USB serial downloader mode

- Download U-Boot image `imx8mn_bsh_smm_s2pro_uboot.bin`, which can be found
  under `firmware/u-boot/imx8mn_bsh_smm_s2pro/` directory of the relevant release
  on the [Apertis image download](https://images.apertis.org/) site.
- Plug the USB type A to micro B cable into the USB Debug Connector (DBG UART).
  Use serial port settings 115200 8N1 to access the debug console.
- Plug another USB type A to micro B cable into the USB-OTG Connector (USB1).
  This connection is used to flash the board firmware using Freescale/NXP
  [UUU tool](https://github.com/NXPmicro/mfgtools).
- In order to boot the board in *USB serial downloader* mode, connect a cable to
  short 3.3V pin (V\_3V3) and boot mode pad (BOOT\_MODE\_0) as shown in the
  following picture:

![iMX8MN BSH SMM S2 PRO Boot Mode](/images/imx8mn_bsh_smm_s2pro_boot_mode.jpg)

- Connect the power supply/adaptor to the DC Power Jack (labelled +5V). While
  keeping the boot mode short from previous step, power up the board by
  switching on the Power ON Switch, which is placed right next to the DC Jack.
  After a few secs, the boot mode short can be released.
- Board is now in *USB serial downloader* mode. On your computer, UUU should
  detect the device:

```
$ uuu -lsusb
uuu (Universal Update Utility) for nxp imx chips -- libuuu_1.4.149-15-g7e20ea1

Connected Known USB Devices
	Path	 Chip	 Pro	 Vid	 Pid	 BcdVersion
	==================================================
	3:423	 MX815	 SDPS:	 0x1FC9	0x013E	 0x0001
```

- Flash the downloaded U-Boot image directly to board's memory. On your
  computer, run:

```
$ uuu imx8mn_bsh_smm_s2pro_uboot.bin
uuu (Universal Update Utility) for nxp imx chips -- libuuu_1.4.149-15-g7e20ea1

Success 1    Failure 0
3:423    2/ 2 [Done                                  ] SDPS: done
```

- Board should run U-Boot bootloader showing the following output on the debug
  serial console:

```
U-Boot SPL 2021.07+dfsg-1+apertis5bv2022preb1 (Oct 27 2021 - 17:58:04 +0000)
WDT:   Not starting
Trying to boot from BOOTROM
Find FIT header 0x&48034800, size 948
Download 699912, total fit 700936


U-Boot 2021.07+dfsg-1+apertis5bv2022preb1 (Oct 27 2021 - 17:58:04 +0000)

CPU:   Freescale i.MX8MNano Quad rev1.0 at 1200 MHz
Reset cause: POR
Model: BSH SMM S2 PRO
DRAM:  512 MiB
WDT:   Started with servicing (60s timeout)
MMC:   FSL_SDHC: 0
Loading Environment from nowhere... OK
In:    serial
Out:   serial
Err:   serial

 BuildInfo:
  - ATF

Net:   CPU Net Initialization Failed
No ethernet found.
Hit any key to stop autoboot:  0
> version
U-Boot 2021.07+dfsg-1+apertis5bv2022preb1 (Oct 27 2021 - 17:58:04 +0000)

gcc (Apertis 10.2.1-6+apertis3bv2022preb4) 10.2.1 20210110
GNU ld (GNU Binutils for Apertis) 2.35.2
```

## U-Boot UUU/fastboot

- [Download]({{< ref "download.md" >}}) the required Apertis image and unzip it.
- In U-Boot shell, enter fastboot mode:

```
> fastboot usb 0
```

- On your computer, run UUU to flash both U-Boot and Apertis image to eMMC:

```
$ uuu -b emmc_all imx8mn_bsh_smm_s2pro_uboot.bin apertis_v2022pre-fixedfunction-arm64-uboot_20211029.0019.img
uuu (Universal Update Utility) for nxp imx chips -- libuuu_1.4.149-15-g7e20ea1

Success 1    Failure 0


3:423    8/ 8 [Done                                  ] FB: done
```

- In U-Boot, fastboot output log should look like this:

```
switch to partitions #0, OK
mmc0(part 0) is current device
Starting download of 16769064 bytes
..........................................................................
.....................................................
downloading of 16769064 bytes finished
** Bad device specification mmc all **
Couldn't find partition mmc all
Flashing sparse image at offset 0
Flashing Sparse Image
........ wrote 16769024 bytes to 'all'
[...]
downloading of 1063824 bytes finished
** Bad device specification mmc bootloader **
Couldn't find partition mmc bootloader
Flashing Raw Image
........ wrote 1063936 bytes to 'bootloader'
```

- Once the firmware has been flashed, reset the board by powering off and on
  again. BSH SMM S2 PRO should now boot smoothly to Apertis.

<!-- end list -->
