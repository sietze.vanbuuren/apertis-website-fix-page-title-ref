+++
date = "2021-10-07"
weight = 100
toc = true

title = "How to Build Your First Image on Apertis"
+++

This guide aims to explain how to build your first image on Apertis
using the GitLab UI. A comprehensive documentation to manually build
a customized image is available at [Image building]( {{< ref "image_building.md" >}} ).

This document will explain how to create a GitLab account and
how to use its UI and pipelines to modify and create a custom Apertis
image.

# Create a GitLab account

To build your Apertis image, you first need an account on the
[Apertis GitLab](https://gitlab.apertis.org) instance.

To create one, please go on at <https://gitlab.apertis.org>, then click on `Sign in / Register`:

![](/images/apertis_gitlab_website.png)

On the next webpage, you will have a `Register now` button:
![](/images/apertis_gitlab_signin.png)

Fill the form, then click on `Register`:
![](/images/apertis_gitlab_signup.png)

At this step, you will have to wait until an administrator approves
your registration:

`
You have signed up successfully. However, we could not sign you in
because your account is awaiting approval from your GitLab administrator.
`

Once your account is approved, you will be notified through an email entitled
`Welcome to GitLab` from `<gitlab at gitlab.apertis.org>`.

The next step is to set up your account. A first useful configuration is to add
your SSH key to be able to push/pull without having to give your
username/password for each action.
For this, you need to go into `Preferences`:
![](/images/apertis_gitlab_preferences.png)

Then, into the `SSH Keys` part:
![](/images/apertis_gitlab_ssh.png)
The instructions [to generate a new key](https://gitlab.apertis.org/help/ssh/index#generate-an-ssh-key-pair)
or [to add an existing one](https://gitlab.apertis.org/help/ssh/index#see-if-you-have-an-existing-ssh-key-pair)
are available.
Please follow the relevant one to configure your SSH key.

# Fork a repository

It is now possible to fork a repository using the GitLab UI.
By forking a repository, you will copy a repository under your
GitLab namespace. That means you will be able to modify files of
the newly forked project, and then later you can eventually do a
`Merge request` to send your changes back to the *upstream* (or original) project.

For this example, we will fork the `image-recipes-sample` example
repository at <https://gitlab.apertis.org/demo/image-recipes-sample>.
This repository contains a recipe to build a custom Apertis image for a
*Raspberry Pi* board.

Click on the `Fork` button:
![](/images/apertis_gitlab_repo_example.png)

A form will be displayed, where you can add information regarding your fork:
![](/images/apertis_gitlab_repo_fork.png)

By clicking on `Fork project`, you will engage the fork process
that it is confirmed by a message saying `Forking in progress`.
Then, after few seconds (or minutes depending of the size of the
repository), you will get a `The project was successfully forked`
message.
![](/images/apertis_gitlab_repo_my_fork.png)

# Add new package to this image recipe

In this example, we will add some `.deb` packages into our first customized
Apertis image.

The recipe to generate an image is a `.yaml` file that is read and
used by [debos](https://github.com/go-debos/debos) to build the image.
The complete syntax of the debos `.yaml` is available on its
[GitHub repository](https://github.com/go-debos/debos)
or you can have a look at its [manpage](https://manpages.debian.org/unstable/debos/debos.1.en.html).

We will modify the `image-rpi64.yaml` file in our just forked
`image-recipes-sample` repository. Scroll down until you see the
`image-rpi64.yaml` file:
![](/images/apertis_gitlab_repo_scrolldown.png)

Then, click on it and on the `Edit` button:
![](/images/apertis_gitlab_repo_edit.png)

For our example, we will add two additional `.deb` packages
(`fonts-dejavu` and `flac`) to the image, for this we need to add the
following section in `image-rpi64.yaml`.
``` yaml
  - action: apt
    description: My additional packages
    packages:
      - fonts-dejavu
      - flac
```
We add it at the end of all *apt* actions and before the first
*run* action.
The order of the different `action` parts is important because these
are run sequentially by `debos`.
![](/images/apertis_gitlab_repo_commit.png)

Add the new section, a commit message and then, click on `Commit changes`.
You should receive a `Your changes have been successfully committed`
message.

# Build an Apertis image

The GitLab CI uses a recipe to build the Apertis Image.
This is configured in the [.gitlab-ci.yml](https://gitlab.apertis.org/demo/image-recipes-sample/-/blob/main/.gitlab-ci.yml)
file located in the root directory of the repository.
Please refer to the official [GitLab CI documentation](https://docs.gitlab.com/ee/ci/)
for more information.

The GitLab CI pipeline will generate a new Apertis Image for
each push (or commit through the UI) into the forked `image-recipes-sample`
repository. If needed, it is possible to generate an image on demand by
selecting `CI/CD`, then `Pipelines` in the sidebar.

![](/images/apertis_gitlab_repo_pipelines.png)

Then, click on `Run pipeline`, select on which branch the pipeline
should be run against and eventually some variables can be defined.
Finally, after another click on `Run pipeline`, the process will start.

The progress of the building can be followed in `CI/CD`, then `Pipelines`:

![](/images/apertis_gitlab_repo_pipeline_new_running.png)

Once all jobs ran successfully (i.e. they should be in green),
it is possible to download the generated image by clicking on the
last job `apt-image-build...`.
![](/images/apertis_gitlab_repo_pipeline_jobs.png)

Then, on `Download` at the right panel:
![](/images/apertis_gitlab_repo_pipeline_download.png)

The image is also available from an URL, in our example we can
download the last image built for the original `images-recipe-sample` repository at
<https://gitlab.apertis.org/demo/image-recipes-sample/-/jobs/artifacts/main/download?job=apt-image-build-arm64-rpi64_containers-rpi64>.

Please refer to these [instructions]( {{< ref "rpi4_setup.md" >}} )
to setup and test your newly built image.
